const fs = require('fs');
const path =    require ('path');
const readline = require('readline')

fs.watch(path.join(__dirname, `./dist`), (eventType, fileName) => {
    const fileWhatWasChanged = path.join(__dirname, './dist', fileName);
 
    // read file
    const content = fs.readFileSync(fileWhatWasChanged);

    const parsedData = JSON.parse(content.toString());
    switch (parsedData.type) {
        case 'gas':
            fs.appendFile(
                path.join(__dirname, './audit/gas.csv'), `\n${parsedData.date},${parsedData.value},${parsedData.name}`
            ,
            () => console.log('gas writen'));
            break;
            case 'water':
                fs.appendFile(
                    path.join(__dirname, './audit/water.csv'), `\n${parsedData.date},${parsedData.value},${parsedData.name}`
                ,
                () => console.log('water writen'));
                break;
    }
})