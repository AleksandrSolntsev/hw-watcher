const fs = require("fs");

const readline = require("readline");

const path = require("path");
///Import

const typeStat = (type) => {
  const statistics = new Map();

  return new Promise((resolve, reject) => {
    const readGas = readline
      .createInterface({
        input: fs.createReadStream(
          path.join(__dirname, `./audit/${type}.csv`)
        ),
      })
      .on("line", (line) => {
        const row = line.split(",");
        if (row.length < 3) {
          return;
        }

        let prevState = statistics.get(row[2].toLowerCase());

        if (prevState) {
          prevState.count++;
          prevState.stats.push({ data: row[1], date: row[0] });
        } else {
           statistics.set(row[2].toLowerCase(), 
        {
            count: 1,
            stats: [{ data: row[1], date: row[0] }],
          });
        }
      })
      .on("close", () => {
        resolve(statistics);
      });
  });
};

const StatOutput = (title, statisticMap) => {
  console.log(`\n${title} : `);
  statisticMap.forEach((value, key, map) => {
    console.log(`User ${key} - внес ${value.count} раз данные `);
    value.stats.forEach((stat) =>
      console.log(`\t ${stat.date} ---- ${stat.data}`)
    );
  });
};

(async () => {
  const [gasStat, waterStat] = await Promise.all([
    typeStat("gas"),
    typeStat("water"),
  ]);

  StatOutput("Gas stats", gasStat);
  StatOutput("Water stats", waterStat);
})();
